import { useState } from "react";
import { Ingredient } from "../../interfaces/ingredient.interface";
import { IngredientField } from "./ingredient-field.component";
import { logEvent } from "firebase/analytics";
import { analytics } from "../../core/firebase";

interface Props {
  onIngredient(ingredient: Ingredient): void;
}
export const NewIngredient = (props: Props) => {
  const [newIngredientName, setNewIngredientName] = useState<string[]>([]);
  const [newIngredientPercentage, setNewIngredientPercentage] =
    useState<number>(0);

  const addIngredient = (e: any) => {
    e.preventDefault();
    const newIngredient: Ingredient = {
      name: newIngredientName[0] ?? "",
      percentage: newIngredientPercentage,
      weight: 0,
      id: Date.now().toString(),
    };
    setNewIngredientName(newIngredientName);
    setNewIngredientPercentage(0);

    logEvent(analytics, "New Ingredient", { name: newIngredient.name });
    props.onIngredient(newIngredient);
  };
  return (
    <div className="card">
      <div className="card-body">
        <div className="row align-items-end">
          <div className="col-auto">
            <label className="form-label">Nombre del ingrediente</label>
            <IngredientField
              selectedIngredient={newIngredientName}
              onSelectedIngredient={function (selected: string[]): void {
                setNewIngredientName(selected);
              }}
            />
          </div>

          <div className="col-auto">
            <label className="form-label">Porcentaje</label>
            <input
              className="form-control"
              type="text"
              inputMode="numeric"
              value={newIngredientPercentage}
              onChange={(e) => {
                const inputValue = e.target.value;
                if (/^\d*$/.test(inputValue)) {
                  setNewIngredientPercentage(+inputValue);
                }
              }}
              placeholder="Porcentaje del ingrediente"
            />
          </div>
          <br />
          <div className="col-auto cl-md-12">
            <label className="form-label">&nbsp;</label>
            <div className="d-grid gap-2">
              <button className="btn btn-primary" onClick={addIngredient}>
                Añadir Ingrediente
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
