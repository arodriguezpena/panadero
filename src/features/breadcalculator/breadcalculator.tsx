import { useEffect, useState } from "react";
import { NewIngredient } from "./new-ingredient";
import { Ingredient } from "../../interfaces/ingredient.interface";
import { OnBoardingComponent } from "./onboarding.component";
import { BreadComponent } from "./bread.component";
import { ListComponent } from "./list.component";
import { logEvent } from "firebase/analytics";
import { analytics } from "../../core/firebase";

const BreadCalculator = () => {
  const [flour, setFlour] = useState<number>(1000);
  const [ingredients, setIngredients] = useState<Ingredient[]>([]);

  useEffect(() => {
    logEvent(analytics, "New user");
  }, []);
  useEffect(() => {
    let updatedIngredients: Ingredient[] = [...ingredients];
    updatedIngredients = updatedIngredients.map((ingredient) => ({
      ...ingredient,
      weight: (flour * ingredient.percentage) / 100,
    }));
    logEvent(analytics, "change flour", { weight: flour });

    setIngredients([...updatedIngredients]);
  }, [flour]);

  return (
    <div>
      <div className="container">
        <h1 className="display-1">
          Calculadora del Porcentaje Panadero Online
        </h1>
        <p className="lead">
          Esta herramienta te permite calcular fácilmente las cantidades de los
          ingredientes para hacer pan basándose en el método del porcentaje
          panadero.
        </p>

        <div className="row">
          <OnBoardingComponent />
        </div>
        <br />
        <div className="row">
          <BreadComponent flour={flour} setFlour={setFlour} />
        </div>
        <br />
        <div className="row">
          <NewIngredient
            onIngredient={(ingredient: Ingredient) => {
              let updatedIngredients: Ingredient[] = [
                ...ingredients,
                ingredient,
              ];
              updatedIngredients = updatedIngredients.map((ingredient) => ({
                ...ingredient,
                weight: (flour * ingredient.percentage) / 100,
              }));

              setIngredients([...updatedIngredients]);
            }}
          />
        </div>
        <br />
        <div className="row">
          <ListComponent
            ingredients={ingredients}
            delete={(ingredient: Ingredient) => {
              const updatedIngredients = ingredients.filter(
                (item) => item.id !== ingredient.id
              );
              setIngredients(updatedIngredients);
            }}
          />
        </div>

        <footer className="text-center">
          <img src="logo.png" alt="" height={48} width={48} />
        </footer>
      </div>
    </div>
  );
};

export default BreadCalculator;
