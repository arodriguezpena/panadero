import { logEvent } from "firebase/analytics";
import { analytics } from "../../core/firebase";

export const OnBoardingComponent = () => {
  return (
    <div className="card text-dark bg-warning border-danger">
      <div className="card-body">
        <div className="accordion accordion-flush" id="accordionFlushExample">
          <div className="accordion-item">
            <h2 className="accordion-header" id="flush-headingOne">
              <button
                className="accordion-button collapsed"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#flush-collapseOne"
                aria-expanded="false"
                aria-controls="flush-collapseOne"
                onClick={() => {
                  logEvent(analytics, "open tour");
                }}
              >
                Haz click aquí y te explicamos cómo usarla:
              </button>
            </h2>
            <div
              id="flush-collapseOne"
              className="accordion-collapse collapse"
              aria-labelledby="flush-headingOne"
              data-bs-parent="#accordionFlushExample"
            >
              <div className="accordion-body">
                <ol>
                  <li>
                    <p>
                      <strong> Introduce la cantidad de harina:</strong> En el
                      campo "Cantidad de harina", introduce la cantidad de
                      harina que vas a utilizar en <mark>gramos</mark>. En el
                      porcentaje panadero,{" "}
                      <mark>
                        la cantidad de harina siempre representa el 100%
                      </mark>{" "}
                      .
                    </p>
                  </li>
                  <li>
                    <p>
                      <strong>Añade los ingredientes:</strong> En los campos de
                      "Nombre del ingrediente" y "Porcentaje del ingrediente",
                      introduce el nombre del ingrediente y el porcentaje que
                      representará en relación a la cantidad de harina.
                    </p>

                    <blockquote className="blockquote">
                      <p>
                        Por ejemplo, si vas a utilizar agua y sabes que
                        necesitas el 60% de la cantidad de harina, introduce
                        "Agua" como el nombre del ingrediente y "60" como el
                        porcentaje. Luego, haz clic en "Añadir Ingrediente".
                        Puedes añadir tantos ingredientes como necesites para tu
                        receta.
                      </p>
                    </blockquote>
                  </li>
                </ol>
                <p className="lead">
                  ¡Y eso es todo! Ahora puedes utilizar estas cantidades para
                  hacer tu pan. Recuerda que el porcentaje panadero es una guía
                  y puedes ajustar los porcentajes según tus necesidades y
                  preferencias.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
