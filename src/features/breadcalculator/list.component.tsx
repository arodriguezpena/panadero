import { logEvent } from "firebase/analytics";
import { Ingredient } from "../../interfaces/ingredient.interface";
import { analytics } from "../../core/firebase";

interface Props {
  ingredients: Ingredient[];
  delete(ingredient: Ingredient): void;
}

export const ListComponent = (props: Props) => {
  const mapItem = () => {
    return props.ingredients.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.name}</td>
          <td>{item.percentage}%</td>
          <td>{item.weight}gr</td>
          <td>
            <button
              className="btn btn-warning"
              onClick={() => {
                logEvent(analytics, "Delete ingredient", {
                  name: item.name,
                  percentage: item.percentage,
                  weight: item.weight,
                });
                props.delete(item);
              }}
            >
              X
            </button>
          </td>
        </tr>
      );
    });
  };

  if (props.ingredients.length === 0) {
    return null;
  }
  return (
    <div className="card">
      <div className="card-body">
        <table className="table table-striped ">
          <thead>
            <tr>
              <th>Ingrediente</th>
              <th>Porcentaje</th>
              <th>Peso</th>
              <th></th>
            </tr>
          </thead>
          <tbody>{mapItem()}</tbody>
        </table>
      </div>
    </div>
  );
};
