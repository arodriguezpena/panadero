interface Props {
  flour: number;
  setFlour(flour: number): void;
}
export const BreadComponent = (props: Props) => {
  return (
    <div className="card">
      <div className="card-body">
        <h3>Cantidad de Harina</h3>
        <div className="input-group col-3">
          <input
            className="form-control form-control-lg"
            type="text"
            id="qtyText"
            inputMode="numeric"
            value={props.flour}
            onChange={(e) => {
              const inputValue = e.target.value;
              if (/^\d*$/.test(inputValue)) {
                props.setFlour(+inputValue);
              }
            }}
            placeholder="Cantidad de harina"
          />
          <span className="input-group-text" id="basic-addon2">
            gramos
          </span>
        </div>
      </div>
    </div>
  );
};
