import { useState } from "react";
import { Typeahead } from "react-bootstrap-typeahead";
import "react-bootstrap-typeahead/css/Typeahead.css";
import { IngredientData } from "./ingredients.data";

interface Props {
  selectedIngredient: string[];
  onSelectedIngredient: (selected: any[]) => void;
}
export const IngredientField = (props: Props) => {
  return (
    <Typeahead
      id="ingredient-input"
      options={IngredientData}
      placeholder="Ingrediente"
      selected={props.selectedIngredient}
      onChange={props.onSelectedIngredient}
      newSelectionPrefix="Nuevo ingrediente: "
      allowNew
    />
  );
};
