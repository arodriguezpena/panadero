import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";

const firebaseConfig = {
  apiKey: "AIzaSyBHgLNaQL79eWv61Bk9Bp-2MRBcC_6At6g",
  authDomain: "panadero-880e1.firebaseapp.com",
  projectId: "panadero-880e1",
  storageBucket: "panadero-880e1.appspot.com",
  messagingSenderId: "792215149954",
  appId: "1:792215149954:web:163b41b22da888bc53839b",
  measurementId: "G-673BDBV5JF",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Initialize Analytics and get a reference to the service
export const analytics = getAnalytics(app);
