export interface Ingredient {
  id: string;
  name: string;
  percentage: number;
  weight: number;
}
